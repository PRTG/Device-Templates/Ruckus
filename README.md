PRTG Device Template for Ruckus Wireless devices
===========================================

This project contains all the files necessary to integrate the Aruba
into PRTG for auto discovery and sensor creation.

Please use this link to download [Ruckus Wireless Template Install package](https://gitlab.com/PRTG/Device-Templates/Ruckus/-/jobs/artifacts/master/download?job=PRTGDistZip)

PRTG Device Templates

PRTG Network monitor can create devices and sensors using templates for any device.
The template files identifies devices by matching checks to what's expected by a device type.

This template is based on the Ruckus MIBs (RUCKUS-*-MIB, RUCKUS-ZD-*-MIB)

Zone Director Sensors
========
 ![Zone Director Health Sensor](./Images/Ruckus_ZD_Health.png)
"Zone Director Status [name]": Creates one per ZD system.

 ![wLAN AP Sensor](./Images/Ruckus_AP.png)
"wLAN AP: [name] ([model])": Creates one per AP system.

![wLAN SSID Sensor](./Images/Ruckus_SSID.png)
"wLAN SSID [ssid-name] [Description]": Creates one per SSID defined for the system.

Standalone systems Sensors
========
"Ruckus Health [Name]": Standalone AP system, one per system

Notes
=====
The template has been tested with :
    Ruckus Wireless ZD3050